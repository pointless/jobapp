function handleMiddlewares(middlewares, to, from, next) {
  const stack = middlewares.slice(0),
    nextMiddleware = stack.shift()
  if (nextMiddleware === undefined) {
    next()
    return 
  }

  nextMiddleware(to, from, (nextArg) => {
    if (nextArg === undefined) {
      handleMiddlewares(stack, to, from, next)
      return
    }
    next(nextArg)
  })
}

export default function middleware(middlewares) {
  return (to, from, next) => handleMiddlewares(middlewares, to, from, next)
}
