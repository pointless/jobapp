import store from '@/store'

export async function auth(to, from, next) {
  return store.getters['auth/isLoggedIn'] ? next() : next('/login')
}

export async function guest(to, from, next) {
  return store.getters['auth/isLoggedIn'] ? next(from.path) : next()
}
