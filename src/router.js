import Vue from 'vue'
import Router from 'vue-router'
import middleware from './middlewares'
import { auth, guest } from './middlewares/check-auth'
import Landing from './views/Landing.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Home from './views/Home.vue'

Vue.use(Router)


export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: middleware([guest]),
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      beforeEnter: middleware([guest]),
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      beforeEnter: middleware([auth]),
    },
  ]
})
