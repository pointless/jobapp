import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueFire from 'vuefire'
import './firebase'


import '@/assets/less/main.less'
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
UIkit.use(Icons)

Vue.config.productionTip = false
Vue.use(VueFire)

store.dispatch('auth/initialize').then(() => {
  window.vm = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

