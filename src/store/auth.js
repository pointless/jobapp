import { auth } from '@/firebase'


export default {
  namespaced: true,
  state: {
    user: false,
    loading: false,
    error: null,
  },
  getters: {
    isLoggedIn: (state) => !! state.user,
    currentUser: (state) => state.user,
    loading: (state) => state.loading,
    error: (state) => state.error,
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    clearUser(state) {
      state.user = false
    }
  },
  actions: {

    async login({state}, { email, password }) {
      state.error = null
      state.loading = true
      try {
        await auth.signInWithEmailAndPassword(email, password)
      } catch (error) {
        state.error = error.message
        throw error
      } finally {
        state.loading = false
      }
    },
    async register({state}, { email, password }) {
      state.error = null
      state.loading = true
      try {
        await auth.createUserWithEmailAndPassword(email, password)
      } catch (error) {
        state.error = error.message
        throw error
      } finally {
        state.loading = false
      }
    },
    async logout({state}) {
      state.error = null
      state.loading = true
      try {
        await auth.signOut()
      } catch (error) {
        state.error = error.message
        throw error
      } finally {
        state.loading = false
      }
    },
    async initialize({commit}) {
      await new Promise((resolve) => {
        auth.onAuthStateChanged(user => {
          if (user) {
            commit('setUser', user)
          } else {
            commit('clearUser')
          }
          resolve()
        })
      })
    }

  }
}